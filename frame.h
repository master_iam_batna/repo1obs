#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"  // canny
#include "opencv2\opencv.hpp"
#include <opencv2/objdetect/objdetect.hpp>
#include <omp.h> // after enable it from paramatre -> c++ -> langage -> openmp oui
#include <time.h>
#include <iostream>
#include <typeinfo> 

using namespace std;
using namespace cv;
#define Frame_Length 400
#define Frame_Width  600

class Frame {

public :
	int Length , width;
	Mat gray_pic,canny_pic;
	Frame(Mat);
	Mat myhougligne();
	Mat full_hougligne();


	void show(Mat, Mat, Mat, Mat);
	void show1(Mat, Mat);
	void timing(clock_t, clock_t, clock_t, clock_t,double);
	void detect();
	void my_rectangle(Mat,Rect,Scalar);
	
};


