#include "frame.h"

using namespace std;
using namespace cv;

Frame::Frame(Mat x) {
	Length = Frame_Length;       //tol
	width = Frame_Width;

	cvtColor(x , gray_pic, CV_BGR2GRAY);
	Canny(gray_pic, canny_pic, 50, 150);
}

Mat Frame::full_hougligne() {
		Mat out = Mat::zeros(400, 600, CV_8UC3);
		vector<Vec4i> lines;     // template 
		HoughLinesP(canny_pic, lines, 1, CV_PI / 180, 50, 50, 10);
		for (size_t i = 0; i < lines.size(); i++) //lines.size()
		{
			Vec4i l = lines[i];
			if (l[1] > 70 or l[3] > 70) {
				line(out, Point(l[0], l[1] ), Point(l[2], l[3] ), Scalar(0, 0, 255), 3, CV_AA); // draw the line 
			}
		}
		return out;
	}

Mat Frame::myhougligne() {
	Mat cut, out;
	out = Mat::zeros(Frame_Length, Frame_Width, CV_8UC3);
	cut = canny_pic(Rect(20, 250, Frame_Width-40, Frame_Length-250-25)); //shifting
	//out = Mat::zeros(300, 500, CV_8UC3);
	//cut = canny_pic(Rect(20, 250, 560, 125)); //shifting
	vector<Vec4i> lines;     // template 
	HoughLinesP(cut, lines, 1, CV_PI / 180, 50, 50, 10);

		for (size_t i = 0; i < lines.size(); i++) //lines.size()
		{
			Vec4i l = lines[i];
			if (l[1] > 70 or l[3] > 70) {
				line(out, Point(l[0]+20, l[1] + 250), Point(l[2]+20, l[3] + 250), Scalar(0, 0, 255), 3, CV_AA); // draw the line 
			}
		}

		my_rectangle(out, Rect(0, 298, 599, 24), Scalar(255, 255, 255));
		for (int x = 300; x < 600; x++) {
			int pix = (int)out.at<Vec3b>(300, x)[2];
			
			if (pix) {
				string m = to_string(x);
				
				my_rectangle(out, Rect(x , 300, 100, 20), Scalar (0, 255, 0));

				putText(out, m , Point(x,290), FONT_HERSHEY_SIMPLEX,0.5,CV_RGB(255, 255, 255), 1,8);//SIMPLEX,PLAIN, DUPLEX,COMPLEX,TRIPLEX, COMPLEX_SMALL, SCRIPT_SIMPLEX, SCRIPT_COMPLEX, 
				break;
			}
		}
			
	for (int x = 300; x > 0; x--) {
				int pix = (int)out.at<Vec3b>(300, x)[2];
				if (pix) {
					string m = to_string(x);
					my_rectangle(out, Rect(x - 100, 300, 100, 20), Scalar(0, 255, 0)) ;
					putText(out, m, Point(x-30, 290), FONT_HERSHEY_SIMPLEX, 0.5, CV_RGB(255, 255, 255), 1, 8);
					break;
				}
			}
	
	line(out, Point(300, 250), Point(300, 350), Scalar(255, 255, 255), 1, CV_AA);   // draw the vertical line 
	//line(out, Point(0  , 295), Point(599, 295), Scalar(255, 255, 255), 1, CV_AA); // draw the horizontal line 	
	return out;
}
void Frame::detect() {
#pragma omp parallel
	{
#pragma omp for
		for (int x = 0; x < canny_pic.rows - 100; x += 10) {
			for (int y = 0; y < canny_pic.cols - 100; y += 10) {
				Mat f = canny_pic.clone();        // clone frame 
				Rect r(y, x, 100, 100);       //create a Rectangle with top-left vertex at (10,20), of width 40 and height 60 pixels.
				Mat rect_cut = f(r);
				//rectangle(f, r, Scalar(255, 0, 0), 1, 8, 0); //draw the rect defined by r with line thickness 1 and Blue color  scalar for RGB COLOR 1 thiknes 8 type of line
				for (int i = 0; i < rect_cut.rows; i++) {
					for (int j = 0; j < rect_cut.cols; j++) {
						int pix = rect_cut.at<uchar>(i, j);
					}
				}
			}
		}
	}
}

void Frame:: show(Mat x1, Mat x2, Mat x3, Mat x4) {
	
	Mat y1, y2, y, xx2;
	cvtColor(x2, xx2, CV_GRAY2BGR);    // show in RGB

	hconcat(x1, xx2, y1);  // display two horizontal pic in one window 
	hconcat(x3, x4, y2);

	vconcat(y1, y2, y);
	imshow("self-drive", y);

	waitKey(1);
}
void Frame::show1(Mat x1, Mat x4) {

	Mat y1;

	hconcat(x1, x4, y1);  // display two horizontal pic in one window 

	imshow("self-drive", y1);

	waitKey(1);
}
void Frame:: timing(clock_t t1, clock_t t2, clock_t t3, clock_t t4, double fps) {
	
		long time1 = t2 - t1;
		long time2 = t4 - t3;

		cout << "time used in haugline : " << time1 / CLOCKS_PER_SEC << endl;
		cout << "time used in for loop : " << time2 / CLOCKS_PER_SEC << endl;
		cout << "fps time  : " << fps / CLOCKS_PER_SEC << endl;
	
	
	
}
void Frame::my_rectangle(Mat out ,Rect mask ,Scalar bgr) {
	rectangle(out, mask, bgr, 1, 8, 0); //scalar for RGB  1 thiknes 8 type of line

}
