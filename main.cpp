#include "frame.h"

using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
	Mat cut, resize_cut, HoughLines_pic, fullHoughLines_pic;
	clock_t t1, t2, t3;
	namedWindow("self-drive", CV_WINDOW_AUTOSIZE);

#if 0
	cut =imread("self-drive-cap.jpg");
	resize(cut, resize_cut, Size(600, 400), 0, 0, INTER_CUBIC);
	Frame myframe(resize_cut); 

	t1 = clock();
	HoughLines_pic = myframe.myhougligne(); 
	t2 = clock();
	//myframe.detect();
	t3 = clock();
	myframe.timing(t1, t2, t2, t3, 1.5);
	//myframe.show(resize_cut, myframe.canny_pic, HoughLines_pic, HoughLines_pic);
	imshow("self-drive", HoughLines_pic);
	waitKey(0);
#endif

#if 1

	VideoCapture road("self-drive-5s.mp4");
	if (!road.isOpened())
		throw "Error when reading steam_avi";
	for (; ;)
	{
		static bool once = 1;
		road >> cut;  // cut a frame from the camera 
		if (cut.empty())
			break;
		double time3 = road.get(CV_CAP_PROP_FPS);
		//resize(cut, resize_cut, Size(600, 400), 0, 0, INTER_CUBIC);
		resize(cut, resize_cut, Size(Frame_Width, Frame_Length), 0, 0, INTER_CUBIC);
		Frame myframe(resize_cut); // create  a frame object 

		t1 = clock();
		HoughLines_pic = myframe.myhougligne();
		fullHoughLines_pic = myframe.full_hougligne(); // just for show 
		 
		t2 = clock();
		myframe.detect();
		t3 = clock();
		if (once) {
			myframe.timing(t1, t2, t2, t3, time3);
			once = 0;
		}
		//myframe.show(resize_cut, myframe.canny_pic, fullHoughLines_pic, HoughLines_pic);
		myframe.show1(resize_cut, HoughLines_pic);
	}
	waitKey(0); // key press to close window
#endif



}